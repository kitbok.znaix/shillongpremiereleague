import React, { useEffect, useState } from "react";
import axios from "axios";
import Modal from "react-modal";
import AdminPage from "./AdminPage";
Modal.setAppElement("#root");
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    maxHeight: "60%", // Adjust the percentage as needed
  },
};

function UpdateAllTeam() {
  const [modalIsOpen, setIsOpen] = React.useState(false);
  const [teamIdToUpdate, setTeamIdToUpdate] = useState(null); // Store the teamId to update
  const [team, setTeam] = useState([]);
  const [id, setId] = useState("");
  const [teamName, setTeamName] = useState("");
  const [coachPhoto, setCoachPhoto] = useState(null);
  const [teamLogo, setTeamLogo] = useState(null);
  const [teamPhoto, setTeamPhoto] = useState([
    { imageId: 1, imageSrc: "" },
    { imageId: 2, imageSrc: "" },
    { imageId: 3, imageSrc: "" },
  ]);
  const [teamDescription, setTeamDescription] = useState("");
  const [teamCoach, setTeamCoach] = useState("");
  const [coachDescription, setCoachDescription] = useState("");

  const token = localStorage.getItem('token');

  const kitPort =
    "http://192.168.1.48/ShillongPremiereLeague/read/R_AllTeam.php";
  const baseUrl = "http://192.168.1.48/ShillongPremiereLeague/Read/";
  const UpdateApi =
    "http://192.168.1.48/ShillongPremiereLeague/update/U_AllTeam.php";

  // Function to filter teamPhoto based on teamId
  const filterTeamPhotoByTeamId = (teamId) => {
    const selectedTeam = team.find((t) => t.teamId === teamId);
    if (selectedTeam) {
      setTeamPhoto(selectedTeam.teamPhoto);
    }
  };

  const handleLogo = (event) => {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        const base64Data = reader.result;
        setTeamLogo(base64Data);
        console.log("LOGO IMAGE", base64Data);
      };
      reader.readAsDataURL(file);
    }
  };

  const handleCoachPhoto = (event) => {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        const base64Data = reader.result;
        setCoachPhoto(base64Data);
        console.log("Coach IMAGE", base64Data);
      };
      reader.readAsDataURL(file);
    }
  };

  const handleImage = (event, index) => {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        const base64Data = reader.result;
  
        
        setTeamPhoto((prevPhotos) => {
          const updatedPhotos = [...prevPhotos]; // Create a copy of the array
          updatedPhotos[index] = {
            ...updatedPhotos[index], // Copy the existing image object
            imageSrc: base64Data, // Update the image source for the specified index
          };
  
          return updatedPhotos;
        });
      };
      reader.readAsDataURL(file);
    }
  };
  
  

  // // Define a function to update teamPhoto based on imageId
  // const updateTeamPhoto = (imageId, newImageSrc) => {
  //   // Find the index of the photo to be updated
  //   const photoIndex = teamPhoto.findIndex(
  //     (photo) => photo.imageId === imageId
  //   );

  //   if (photoIndex !== -1) {
  //     // Create a copy of the teamPhoto array
  //     const updatedTeamPhoto = [...teamPhoto];

  //     // Update the image source at the found index
  //     updatedTeamPhoto[photoIndex].imageSrc = newImageSrc;

  //     // Update the state with the modified teamPhoto array
  //     setTeamPhoto(updatedTeamPhoto);
  //   } else {
  //     console.error("Image not found with imageId:", imageId);
  //   }
  // };

  
  // You can call this function to update the teamPhoto array
  // For example, to update the image with imageId 5:
  // updateTeamPhoto(5, "new-image-src.jpg");

  const handleSubmit = async (event) => {
    event.preventDefault();
  
    // Create an array of teamPhoto objects in the required format
    const teamPhotoArray = teamPhoto.map((photo) => ({
      imageId: photo.imageId,
      imageSrc: photo.imageSrc, // Remove the array wrapping
    }));
  
    // Create the data object in the required format
    const requestData = {
      id,
      teamName,
      teamDescription,
      teamLogo,
      teamCoach,
      coachDescription,
      coachPhoto,
      teamId: teamIdToUpdate,
      teamPhoto: teamPhotoArray,
    };
  
    try {
      const response = await axios.put(UpdateApi, requestData, {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`, // Include the token in the headers
        },
      });
  
      // Handle the response from the API
      if (response.status === 200 || response.status === 206){
        console.log("Update successful:", response.data);
        // Close the modal after updating
        closeModal();
        window.alert('Updated Successfully')
      } else {
        console.error("Update failed:", response.data);
        // Handle the error as needed
        window.alert('Fail to Update')
      }
    } catch (error) {
      console.error("Error:", error);
      // Close the modal if there's an error
      window.alert('Fail to Update')

      closeModal();
    }
  };
  

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  useEffect(() => {
    axios
      .get(kitPort)
      .then((res) => {
        if (res.data.data && Array.isArray(res.data.data)) {
          setTeam(res.data.data);

          // Extract teamPhoto arrays from each team object and flatten them into one array
          const allTeamPhotos = res.data.data
            .map((team) => team.teamPhoto)
            .flat();

          setTeamPhoto(allTeamPhotos);
        } else {
          console.error("API response does not contain an array:", res.data);
        }
      })
      .catch((err) => console.error(err));
  }, []);

  const handleUpdateClick = async (teamId) => {
    // Set the teamIdToUpdate
    setTeamIdToUpdate(teamId);

    // Call the function to filter teamPhoto based on teamId
    filterTeamPhotoByTeamId(teamId);
 
    // Find the team in the array with the matching teamId
    const teamToUpdate = team.find((team) => team.teamId === teamId);

    if (teamToUpdate) {
      // Populate all the form fields with team data
      setId(teamToUpdate.teamId);
      const imageIds = teamToUpdate.teamPhoto.map((photo) => photo.imageId);

      // Create an array of empty objects with the imageIds
      const emptyTeamPhoto = imageIds.map((imageId) => ({
        imageId,
        imageSrc: "",
      }));
      setTeamPhoto(emptyTeamPhoto);
      setTeamName(teamToUpdate.teamName);
      setTeamCoach(teamToUpdate.teamCoach);
      openModal();
    } else {
      console.error("Team with teamId not found");
      closeModal();
    }
  };

  const handleModalUpdateClick = () => {
    // Create a new FormData object
    const formData = new FormData();
    formData.append("id", teamIdToUpdate);
    if (teamName !== "") {
      formData.append("teamName", teamName);
    }
    if (teamDescription !== "") {
      formData.append("teamDescription", teamDescription);
    }
    if (teamLogo !== null) {
      formData.append("teamLogo", teamLogo);
    }
    if (teamCoach !== "") {
      formData.append("teamCoach", teamCoach);
    }
    if (coachDescription !== "") {
      formData.append("coachDescription", coachDescription);
    }
    if (coachPhoto !== null) {
      formData.append("coachPhoto", coachPhoto);
    }

    
    // Check if teamPhoto needs to be appended
    if (teamPhoto.some((photo) => photo.imageSrc !== null)) {
      teamPhoto.forEach((image, index) => {
     
        if (image.imageSrc !== '') {
          formData.append(`teamPhoto[${index}][imageId]`, image.imageId);
          formData.append(`teamPhoto[${index}][imageSrc]`, image.imageSrc);
        }
      });
    }

    // Perform the update operation based on teamIdToUpdate
    if (teamIdToUpdate) {
      axios
        .put(UpdateApi, formData, {
          headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`,
          },
        })
        .then((res) => {
          // Handle the response if needed
          console.log(res);
          window.alert('Team updated ')
          console.log("Update successful");
          // Close the modal after updating
          closeModal();
        })
        .catch((error) => {
          console.error("Error:", error);
          window.alert('Fail to update')
          // Close the modal if there's an error
          closeModal();
        });
    }
  };

  return (
    <div>
      <AdminPage/>
      <div className="grid-container">
        <div className="grid grid-cols-3 gap-4 mx-40 my-20">
          {team.map((item, index) => (
            <div
              key={`${item.id}-${index}`}
              className="bg-white rounded-md shadow-md p-4"
            >
              <img
                src={`${baseUrl}${item.teamLogo}`}
                alt="Team Logo"
                className="w-full h-28"
              />
              <div className="text-lg font-semibold">{item.teamName}</div>
              {/* Add the onClick event to call handleUpdateClick */}
              <button onClick={() => handleUpdateClick(item.teamId)}>
                Manage
              </button>
            </div>
          ))}
        </div>
      </div>

      {/* Modal for update */}
      {teamIdToUpdate && (
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Update Team Modal"
        >
          <button onClick={closeModal} className="float-right">
            ❌
          </button>
          <div>
            <form onSubmit={handleSubmit} className="space-y-4">
              <div className="mb-4">
                <input
                  placeholder="ID"
                  type="text"
                  value={id}
                  readOnly
                  className="border border-gray-300 p-2 rounded"
                />
              </div>
              <div className="mb-4">
                <input
                  placeholder="Team Name"
                  type="text"
                  value={teamName}
                  onChange={(e) => setTeamName(e.target.value)}
                  className="border border-gray-300 p-2 rounded"
                />
              </div>
              <div className="mb-4">
                <label>Upload Team Logo</label>
                <br />
                <input
                  type="file"
                  accept="image/*"
                  onChange={(e) => handleLogo(e)}
                  name="teamLogo"
                  className="border border-gray-300 p-2 rounded"
                />
                {teamLogo && (
                  <div>
                    <img src={teamLogo} alt="Team Logo" className="w-10 h-10" />
                  </div>
                )}
              </div>
              <div className="mb-4">
                <label>Upload Coach Photo</label>
                <br />
                <input
                  type="file"
                  accept="image/*"
                  onChange={(e) => handleCoachPhoto(e)}
                  name="coachPhoto"
                  className="border border-gray-300 p-2 rounded"
                />
                {coachPhoto && (
                  <div>
                    <img
                      src={coachPhoto}
                      alt="Coach Photo"
                      className="w-10 h-10"
                    />
                  </div>
                )}
              </div>
              <div>
                {teamPhoto.map((image, index) => (
                  <div key={index} className="mb-4">
                    <input
                      placeholder={`Image ${image.imageId} ID`}
                      type="text"
                      value={image.imageId}
                      readOnly
                      className="border border-gray-300 p-2 rounded"
                    />
                    <input
                      placeholder={`Image ${image.imageId} URL`}
                      type="file"
                      onChange={(e) => handleImage(e, index)}
                      className="border border-gray-300 p-2 rounded"
                    />
                  </div>
                ))}
              </div>
              <div>
                <input
                  placeholder="Team Description"
                  type="text"
                  name="teamDescription"
                  value={teamDescription}
                  className="border border-gray-300 p-2 rounded"
                  onChange={(e) => setTeamDescription(e.target.value)}
                />
              </div>
              <div>
                <input
                  placeholder="Coach Name"
                  type="text"
                  name="teamCoach"
                  value={teamCoach}
                  className="border border-gray-300 p-2 rounded"
                  onChange={(e) => setTeamCoach(e.target.value)}
                />
              </div>
              <div>
                <textarea
                  placeholder="Coach description"
                  type="textarea"
                  name="coachDescription"
                  value={coachDescription}
                  className="border border-gray-300 p-2 rounded"
                  onChange={(e) => setCoachDescription(e.target.value)}
                />
              </div>
            </form>
          </div>
          {/* Add your update form or content here */}
          <center>
            <button
              onClick={handleModalUpdateClick}
              className="bg-blue-500 text-white py-2 px-4 rounded mt-2"
            >
              Update
            </button>
          </center>
        </Modal>
      )}
    </div>
  );
}
export default UpdateAllTeam;
