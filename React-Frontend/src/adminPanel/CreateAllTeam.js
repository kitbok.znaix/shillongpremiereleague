import React, { useState } from "react";
import axios from "axios";
import AdminPage from "./AdminPage";

function CreateAllTeam() {
  const [teamName, setTeamName] = useState("");
  const [teamLogo, setTeamLogo] = useState(null);
  const [teamPhoto, setteamPhoto] = useState({
    teamPhoto: ["", "", ""],
  });
  const [teamDescription, setTeamDescription] = useState("");
  const [teamCoach, setTeamCoach] = useState("");
  const [coachDescription, setCoachDescription] = useState("");
  const [coachPhoto, setCoachPhoto] = useState(null);
  const kitPort =
    "http://192.168.1.48/ShillongPremiereLeague/create/C_AllTeam.php";


  const handleLogo = (event) => {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        const base64Data = reader.result;
        setTeamLogo(base64Data);
      };
      reader.readAsDataURL(file);
    }
  };

  const handleImage = (event, index) => {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        const base64Data = reader.result;
        setteamPhoto((prevImages) => ({
          ...prevImages,
          teamPhoto: [
            ...prevImages.teamPhoto.slice(0, index),
            base64Data,
            ...prevImages.teamPhoto.slice(index + 1),
          ],
        }));
      };
      reader.readAsDataURL(file);
    }
  };
  const handleCoachPhoto = (event) => {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        const base64Data = reader.result;
        setCoachPhoto(base64Data);
      };
      reader.readAsDataURL(file);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Create a JavaScript object to represent the form data
    const formData = {
      teamName,
      teamLogo,
      teamPhoto: teamPhoto.teamPhoto,
      teamDescription,
      teamCoach,
      coachDescription,
      coachPhoto,
    };

    // Retrieve the token from local storage
    const token = localStorage.getItem("token");

    // Create an Axios instance with the token
    const axiosInstance = axios.create({
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    try {
      // Convert the JavaScript object to JSON
      const jsonData = JSON.stringify(formData);

      // Make the POST request with the JSON data
      const response = await axiosInstance.post(kitPort, jsonData, {
        headers: {
          "Content-Type": "application/json", // Set the content type to JSON
        },
      });

      // Handle the response as needed
      console.log("Response from server:", response.data);

      // Check if the request was successful (you may need to adjust the condition)
      if (response.status === 200) {
        // Show a success message
        window.alert("Team created successfully!");
      }
    } catch (error) {
      // Handle errors
      console.error("Error:", error);
    }
  };

  return (
    <div className="p-4">
      <AdminPage />
      <h1 className="text-2xl font-bold mb-4">Create All Team</h1>
      <center>
        <div className="flex flex-col items-center">
        <form onSubmit={handleSubmit} className="space-y-4">
          <div className="mb-4">
            <label className="block">Name:</label>
            <input
              type="text"
              value={teamName}
              onChange={(e) => setTeamName(e.target.value)}
              className="border border-gray-300 p-2 rounded"
              required
            />
          </div>
          <div className="mb-4">
            <label className="block">Team Description:</label>
            <input
              type="text"
              value={teamDescription}
              onChange={(e) => setTeamDescription(e.target.value)}
              className="border border-gray-300 p-2 rounded"
              required
            />
          </div>
      
          <div className="mb-4">
            <label className="block">Team Coach Name:</label>
            <input
              type="text"
              value={teamCoach}
              onChange={(e) => setTeamCoach(e.target.value)}
              className="border border-gray-300 p-2 rounded"
              required
            />
          </div>
          <div className="mb-4">
            <label className="block">Coach Description:</label>
            <input
              type="text"
              value={coachDescription}
              onChange={(e) => setCoachDescription(e.target.value)}
              className="border border-gray-300 p-2 rounded"
              required
            />
          </div>
          <div className="mb-4">
            <label className="block">Logo:</label>
            <input
              type="file"
              accept="image/*"
              onChange={handleLogo}
              name="teamLogo"
              className="border border-gray-300 p-2 rounded"
              required
            />
          </div>
          <div className="mb-4">
            <label className="block">Coach Photo:</label>
            <input
              type="file"
              accept="image/*"
              onChange={handleCoachPhoto}
              name="coachPhoto"
              className="border border-gray-300 p-2 rounded"
              required
            />
          </div>
          {teamPhoto.teamPhoto.map((image, index) => (
            <div key={index} className="mb-4">
              <label className="block">{`Image ${index + 1}:`}</label>
              <input
                type="file"
                accept="image/*"
                onChange={(e) => handleImage(e, index)}
                name={`teamPhoto[${index}]`}
                className="border border-gray-300 p-2 rounded"
                required
              />
            </div>
          ))}
          <div>
            <button
              type="submit"
              className="bg-blue-500 text-white py-2 px-4 rounded"
            >
              Create Team
            </button>
          </div>
        </form>
        </div>
      </center>
    </div>
  );
}

export default CreateAllTeam;
