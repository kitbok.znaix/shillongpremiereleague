import React, { useState } from "react";
import axios from "axios";
import AdminPage from "./AdminPage";

function InsertToGallery() {
  const [gallery, setGallery] = useState({
    photo: ["", "", ""],
  });
  const kitPort =
    "http://192.168.1.48/ShillongPremiereLeague/create/C_Gallery.php";

  const handleImage = (event, index) => {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        const base64Data = reader.result;
        setGallery((prevImages) => ({
          ...prevImages,
          photo: [
            ...prevImages.photo.slice(0, index),
            base64Data,
            ...prevImages.photo.slice(index + 1),
          ],
        }));
      };
      reader.readAsDataURL(file);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const token = localStorage.getItem("token");
    // Create an array of image data objects
    const imageArray = gallery.photo.map((image) => image);

    // Create the JSON data object
    const jsonData = {
      photo: imageArray,
      // Add any other properties you need in your JSON structure
    };

    try {
      const response = await axios.post(kitPort, jsonData, {
        headers: {
          "Content-Type": "application/json", // Set the content type to JSON
          Authorization: `Bearer ${token}`,
        },
      });

      // Handle the response as needed
      console.log("Response from server:", response.data);
      window.alert("Images inserted to the gallery");
    } catch (error) {
      // Handle errors
      console.error("Error:", error);
      window.alert("Fail to inserted Images to the gallery");
    }
  };

  return (
    <div className="p-4">
      <AdminPage />
      <h1 className="text-2xl font-bold mb-4">Insert to Gallery</h1>
      <form onSubmit={handleSubmit}>
        {gallery.photo.map((image, index) => (
          <div key={index} className="mb-4">
            <label className="block mb-2">{`Image ${index + 1}:`}</label>
            <input
              type="file"
              accept="image/*"
              onChange={(e) => handleImage(e, index)}
              name={`photo[${index}]`}
              className="border border-black py-2 px-3 rounded"
            />
          </div>
        ))}
        <div>
          <button
            type="submit"
            className="bg-blue-500 text-white py-2 px-4 rounded"
          >
            Insert
          </button>
        </div>
      </form>
    </div>
  );
}

export default InsertToGallery;
