import React from "react";
import { Outlet } from "react-router-dom";

function MenuForGallery() {
  return (
    <div>
      <h1>Gallery Menu</h1>
      <Outlet /> 
    </div>
  );
}

export default MenuForGallery;
