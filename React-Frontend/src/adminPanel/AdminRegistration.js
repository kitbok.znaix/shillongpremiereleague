import axios from 'axios';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
function AdminRegistration() {
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
    const navigate = useNavigate()
  const kitPort = 'http://192.168.1.48/ShillongPremiereLeague/create/C_User.php';

  const handleSubmit = (e) => {
    const token = "eyJhbGdvIjoiSFMyNTYiLCJ0eXBlIjoiSldUIn0=.eyJJRCI6MTUsIlVzZXJOYW1lIjoiYWRtaW4iLCJ0aW1lIjoxNjk1MjcyOTIzfQ==.YWY2MTk0NjFiZTIwZWRjNGIwNzgyYTMyMjk1YmIxNjA4YzMzNWU3MTE4ZjBhMmI2OWEwZTNjNzYyOTcyYWI1ZA=="
    e.preventDefault();
    const postData = {
      userName: userName,
      password: password,
    };

    axios
      .post(kitPort, postData, {
        headers: {
          'Content-Type': 'application/json', // Set the content type to JSON
          Authorization :`Bearer ${token}`
        },
      })
      .then((res) => {
        // Handle the response here, if needed
        if (res.data || res.data.success){
        navigate('/adminLogin')
        console.log(res.data);
        }else{
            console.log("error");
        }
      })
      .catch((err) => {
        // Handle any errors here
        console.error(err);
      });
  };

  return (
    <>
    <h3 className='mt-14'>Admin Registration</h3>
    <div className="flex justify-center items-center h-screen">
      <form onSubmit={handleSubmit} className="text-center">
        <input
          type="text"
          placeholder="User Name"
          className="border-b border-black mb-4 px-4 py-2 rounded"
          value={userName}
          onChange={(e) => setUserName(e.target.value)}
        />
        <br />
        <input
          type="password"
          placeholder="Admin Password"
          className="border-b border-black mb-4 px-4 py-2 rounded"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <br />
        <button type="submit" className="bg-blue-500 text-white px-4 py-2 rounded">
          Submit
        </button>
      </form>
    </div>
    </>
  );
}

export default AdminRegistration;

