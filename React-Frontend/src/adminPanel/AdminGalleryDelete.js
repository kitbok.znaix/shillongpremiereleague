import React, { useEffect, useState } from "react";
import Lightbox from "yet-another-react-lightbox";
import "yet-another-react-lightbox/styles.css";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Download from "yet-another-react-lightbox/plugins/download";
import Share from "yet-another-react-lightbox/plugins/share";
import Counter from "yet-another-react-lightbox/plugins/counter";
import axios from "axios";
import AdminPage from "./AdminPage";

function AdminGalleryDelete() {
  const [index, setIndex] = useState(-1);
  const [gallery, setGallery] = useState([]);
  const kitPort =
    "http://192.168.1.48/ShillongPremiereLeague/read/R_Gallery.php";
  const baseUrl = "http://192.168.1.48/ShillongPremiereLeague/Read/";

  useEffect(() => {
    axios
      .get(kitPort)
      .then((res) => {
        const galleryData = res.data.data;
        const galleryJSON = galleryData.map((photo) => ({
          id: photo.id,
          photoSrc: `${baseUrl}${photo.photo}`,
          title: `Image ${photo.id}`,
        }));

        setGallery(galleryJSON);
      })
      .catch((err) => console.log(err));
  }, []);

  const openLightbox = (index) => {
    setIndex(index);
  };

  const closeLightbox = () => {
    setIndex(-1);
  };

  const handleDelete = (ToDeleteid) => {
    const idToDelete = { id: ToDeleteid };
    const idToDeleteJSON = JSON.stringify(idToDelete);
    const shouldDelete = window.confirm("Are you sure you want to delete this image?");

    if (shouldDelete) {
      // Retrieve the token from local storage
      const token = localStorage.getItem("token");

      // Send a DELETE request to your server with the token in the headers
      axios
        .delete(`http://192.168.1.48/ShillongPremiereLeague/delete/D_Gallery.php`, {
          data: idToDeleteJSON,
          headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
        })
        .then((res) => {
          if (res.status === 200) {
            // Image deleted successfully, update the gallery state
            setGallery((prevGallery) => prevGallery.filter((photo) => photo.id !== ToDeleteid));
            console.log("Image deleted successfully");
          } else {
            console.error("Failed to delete image");
          }
        })
        .catch((err) => {
          console.error("Error:", err);
        });
    }
  };

  return (
    <>
    <AdminPage/>
    <div className="mt-20">
      <div className="grid grid-cols-6 gap-4">
        {gallery.map((photo, index) => (
          <div key={photo.id} className="relative">
            <img
              src={photo.photoSrc}
              alt={photo.title}
              title={photo.title}
              onClick={() => openLightbox(index)}
              className="cursor-pointer object-cover w-full h-full rounded-md"
            />
            <button
              onClick={() => handleDelete(photo.id)}
              className="absolute top-2 right-2 bg-red-500 text-white px-2 py-1 rounded"
            >
              Delete
            </button>
          </div>
        ))}
      </div>

      <Lightbox
        plugins={[Zoom, Fullscreen, Download, Share, Counter]}
        index={index}
        slides={gallery.map((photo) => ({
          src: photo.photoSrc,
          title: photo.title,
        }))}
        open={index >= 0}
        close={closeLightbox}
      />
    </div>
    </>
  );
}

export default AdminGalleryDelete;
