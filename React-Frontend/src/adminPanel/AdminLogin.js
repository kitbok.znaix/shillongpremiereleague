import axios from 'axios';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function AdminLogin() {
    const kitPort = 'http://192.168.1.48/ShillongPremiereLeague/verifyUser.php'; 
    const [userName, setName] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null); // State for handling errors
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {
            userName: userName,
            password: password,
        };

        axios
            .post(kitPort, data)
            .then((res) => {
                console.log(res.data);
                // Assuming the response contains a 'success' field
                if (res.data || res.data.success) {
                    localStorage.setItem('token', res.data.token);
                    console.log(res.data);
                    navigate('/adminPage');
                    // }
                } else {
                    // Handle unsuccessful login here, show an error message, etc.
                    setError("Invalid credentials");
                }
            })
            .catch((err) => {
                console.log('Error:', err);
                setError("An error occurred. Please try again later.");
            });
    };

    return (
        <div className='flex justify-center items-center h-screen'>
            <form onSubmit={handleSubmit} className='text-center'>
                <input
                    type='text'
                    placeholder='Admin User Name'
                    className='border border-gray-300 px-4 py-2 rounded mb-4'
                    value={userName}
                    onChange={(e) => setName(e.target.value)}
                />
                <br />
                <input
                    type='text'
                    placeholder='Admin Password'
                    className='border border-gray-300 px-4 py-2 rounded mb-4'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <br />
                <button type='submit' className='bg-blue-500 text-white px-4 py-2 rounded'>
                    Login
                </button>
                {error && <div className="text-red-500 mt-2">{error}</div>}
            </form>
        </div>
    );
}

export default AdminLogin;
