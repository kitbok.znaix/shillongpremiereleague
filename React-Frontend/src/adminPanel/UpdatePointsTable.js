import React, { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import AdminPage from "./AdminPage";

function UpdatePointsTable() {
  const [club, setClub] = useState([]);
  const [updatePoint, setUpdatePoints] = useState({
    teamId: null,
    teamName: "",
    matchPlay: "",
    win: "",
    lose: "",
    draw: "",
    goalAgainst: "",
    goalFor: "",
    points: "",
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    const token = localStorage.getItem("token");
    // Get the selected teamName from the dropdown
    const selectedId = updatePoint.teamId;

    // Define the fields you want to update
    const fieldsToUpdate = {
      teamId: updatePoint.teamId,
      win: updatePoint.win,
      lose: updatePoint.lose,
      draw: updatePoint.draw,
      goalAgainst: updatePoint.goalAgainst,
      goalFor: updatePoint.goalFor,
      points: updatePoint.points,
      matchPlay:updatePoint.matchPlay
    };

    axios.put(
      `http://192.168.1.48/ShillongPremiereLeague/update/U_Point.php?id=${selectedId}`,
      fieldsToUpdate, // Use the fieldsToUpdate object for the update
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    )
    .then((res) => {
      console.log(res);
      window.alert(res.data.message);
    })
    .catch((err) => {
      console.log(err);
    });
    
  };

  //   axios.
  //   put('http://192.168.101.17/ShillongPremiereLeague/update/U_Point.php',fieldsToUpdate,{
  //     headers:{
  //         "Content-Type": "application/json",
  //           Authorization: `Bearer ${token}`,
  //     },
  //   });
  // }

  useEffect(() => {
    axios
      .get(
        "http://192.168.1.48/ShillongPremiereLeague/read/R_PointUpdate.php"
      )
      .then((res) => setClub(res.data.data))
      .catch((err) => console.log(err));
  }, []);
  return (
    <>
      <AdminPage />
      <h3>Update Points Table</h3>
      <div className="mt-14 p-4">
        <div className="flex flex-col items-center">
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label className="">Select Club:</label>
            <select
              required
              className="border border-black rounded-md p-1"
              onChange={(e) => {
                const selectedOption = club.find(
                  (item) => item.teamName === e.target.value
                );
                if (selectedOption) {
                  // Set both teamName and teamId in the updatePoint state
                  setUpdatePoints({
                    ...updatePoint,
                    teamName: selectedOption.teamName,
                    teamId: selectedOption.teamId, // Set the teamId correctly
                  });
                }
              }}
            >
              <option value="" disabled selected>
                --Select Team--
              </option>
              {club.map((item) => (
                <option key={item.teamId} value={item.teamName}>
                  {item.teamName} - {item.teamId}
                </option>
              ))}
            </select>
          </div>

          <div className="mb-4">
            <label htmlFor="mpInput" className="mr-2">
              MP:
            </label>
            <input
              type="number"
              id="mpInput"
              // value={matchPlay}
              className="border border-black rounded-md p-1"
              onChange={(e) =>
                setUpdatePoints({ ...updatePoint, matchPlay: e.target.value })
              }
            />
          </div>

          <div className="mb-4">
            <label htmlFor="wInput" className="mr-2">
              W:
            </label>
            <input
              type="number"
              id="wInput"
              className="border border-black rounded-md p-1"
              onChange={(e) =>
                setUpdatePoints({ ...updatePoint, win: e.target.value })
              }
            />
          </div>

          <div className="mb-4">
            <label htmlFor="dInput" className="mr-2">
              D:
            </label>
            <input
              type="number"
              id="dInput"
              className="border border-black rounded-md p-1"
              onChange={(e) =>
                setUpdatePoints({ ...updatePoint, draw: e.target.value })
              }
            />
          </div>

          <div className="mb-4">
            <label htmlFor="lInput" className="mr-2">
              L:
            </label>
            <input
              type="number"
              id="lInput"
              className="border border-black rounded-md p-1"
              onChange={(e) =>
                setUpdatePoints({ ...updatePoint, lose: e.target.value })
              }
            />
          </div>

          <div className="mb-4">
            <label htmlFor="gfInput" className="mr-2">
              GF:
            </label>
            <input
              type="number"
              id="gfInput"
              className="border border-black rounded-md p-1"
              onChange={(e) =>
                setUpdatePoints({ ...updatePoint, goalFor: e.target.value })
              }
            />
          </div>

          <div className="mb-4">
            <label htmlFor="gaInput" className="mr-2">
              GA:
            </label>
            <input
              type="number"
              id="gaInput"
              className="border border-black rounded-md p-1"
              onChange={(e) =>
                setUpdatePoints({ ...updatePoint, goalAgainst: e.target.value })
              }
            />
          </div>

          <div className="mb-4">
            <label htmlFor="ptsInput" className="mr-2">
              Pts:
            </label>
            <input
              type="number"
              id="ptsInput"
              className="border border-black rounded-md p-1"
              onChange={(e) =>
                setUpdatePoints({ ...updatePoint, points: e.target.value })
              }
            />
          </div>

          <input
            type="submit"
            value="Submit"
            className="border border-black rounded-md bg-blue-500 text-white p-2 cursor-pointer"
          />
        </form>
        </div>
      </div>
    </>
  );
}

export default UpdatePointsTable;
