import "./App.css";
import RoutesConfig from "./RoutesConfig";
import AdminLogin from "./backend/AdminLogin";
import CreateAllTeam from "./backend/CreateAllTeam";
import InsertToGallery from "./backend/InsertToGallery";
import MyCarousel from "./components/MyCarousel";
import Navbar from "./components/Navbar";
import QuickSearch from "./components/QuickSearch";
import TableAndFixture from "./components/TableAndFixture";
import Footer from "./components/Footer";
import CreateFixture from "./backend/CreateFixture";
import AdminPage from "./backend/AdminPage";
function App() {
  return (
    <div className="App">
      <RoutesConfig />
      {/* <Navbar/> */}
      {/* <QuickSearch/> */}
      {/* <CreateAllTeam/> */}
      {/* <MyCarousel/> */}
      {/* <TestRead/> */}
      {/* <AdminLogin/> */}
      {/* <InsertToGallery/> */}
      {/* <TableAndFixture/> */}
      {/* <Footer/> */}
      {/* <TestGallery/> */}
      {/* <CreateFixture/> */}
      {/* <AdminPage/> */}
    </div>
  );
}

export default App;
