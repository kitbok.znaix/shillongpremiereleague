import React from 'react';
import Navbar from './Navbar';
import QuickSearch from './QuickSearch';
import SearchText from './SearchText';
import Footer from './Footer';
export default function Home() {
  const backgroundImageStyle = {
    backgroundImage: 'url(heroImage.jpg)',
    backgroundSize: 'cover',
    backgroundPosition: 'left',
    minHeight: 'calc(100vh - 56px)', // Adjust 56px according to your Navbar height
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundRepeat: 'no-repeat', // Optionally set background repeat
};


  return (
    <div>
      <div style={backgroundImageStyle}>
        <h2 className="text-white text-4xl">A new era of football in Shillong begins</h2>
      </div>
      <SearchText />
      <QuickSearch />
    </div>
  );
}
