import React, { useState } from "react";
import {  NavLink, useLocation } from "react-router-dom";
import { useNavigate } from "react-router-dom";

function Navbar() {
  const location = useLocation();
  const isHomePage = location.pathname === "/";
  const navigate = useNavigate();
  const [isMenuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!isMenuOpen);
  };

  const closeMenu = () => {
    setMenuOpen(false);
  };

  const navbarClasses = `bg-${isHomePage ? 'transparent' : 'blue-500'} p-2 absolute top-0 right-0 left-0 ${
    isMenuOpen ? 'h-auto' : 'h-12'
  }`;

  const navActive=({isActive})=>{
      return {
        textDecoration : isActive ? "underline" : "none"
      }
  }

  const linkClasses = ` p-2 text-sm ${isMenuOpen ? 'block' : 'hidden'} lg:block md:block`;

  return (
    <nav className={navbarClasses}>
      <div className="container mx-auto">
        <div className="flex justify-between items-center">
          <div onClick={() => navigate("/")}>
            {!isHomePage && (
              <p
                className={`text-2xl font-bold cursor-pointer ${
                  isMenuOpen ? "hidden" : ""
                }`}
              >
                ⚽
              </p>
            )}
          </div>

          <div className="lg:hidden md:hidden">
            <button
              onClick={toggleMenu}
              className="text-white p-2 focus:outline-none"
            >
              {isMenuOpen ? (
                <div>X</div>
              ) : (
                <div> ☰</div>
              )}
            </button>
          </div>

          <ul
            className={`lg:flex md:flex space-x-4 text-white z-20 cursor-pointer justify-end
            ${isMenuOpen ? "" : "hidden"}`}
          >
            {/* Use NavLink for Home link */}
            <li>
              <NavLink
                to="/"
                exact // Exact match for home
                // activeClassName="bg-red-600" // Apply this class when active
                onClick={closeMenu}
                className={linkClasses}
                style={navActive}
              >
                Home
              </NavLink>
            </li>
            {/* Use NavLink for other links */}
            <li>
              <NavLink
                to="/about"
               
                onClick={closeMenu}
                className={linkClasses}
                style={navActive}
              >
                About Us
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/gallery"
               
                onClick={closeMenu}
                className={linkClasses}
                style={navActive}
              >
                Gallery
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/tableAndFixture"
               
                onClick={closeMenu}
                className={linkClasses}
                style={navActive}
              >
                Table And Fixture
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
