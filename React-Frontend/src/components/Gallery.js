import React, { useEffect, useState } from "react";
import Lightbox from "yet-another-react-lightbox";
import "yet-another-react-lightbox/styles.css";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Download from "yet-another-react-lightbox/plugins/download";
import Share from "yet-another-react-lightbox/plugins/share";
import Counter from "yet-another-react-lightbox/plugins/counter";
import "yet-another-react-lightbox/plugins/counter.css";
import "yet-another-react-lightbox/plugins/thumbnails.css";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import Slideshow from "yet-another-react-lightbox/plugins/slideshow";
import axios from "axios";

function Gallery() {
  const [index, setIndex] = useState(-1);
  const [gallery, setGallery] = useState([]);
  const kitPort =
    "http://192.168.1.48/ShillongPremiereLeague/read/R_Gallery.php";
  const baseUrl = "http://192.168.1.48/ShillongPremiereLeague/Read/";

  useEffect(() => {
    axios
      .get(kitPort)
      .then((res) => {
        setGallery(res.data.data); // Extract 'data' array from the response
        console.log(res.data.data); // Log the data received from the API
      })
      .catch((err) => console.log(err));
  }, []);

  const openLightbox = (index) => {
    setIndex(index);
  };

  const closeLightbox = () => {
    setIndex(-1);
  };

  return (
    <div className="mt-20">
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-x-2 gap-y-4">
        {gallery.map((photo, index) => (
          <div key={index} className="custom-gallery-item">
            <img
              src={`${baseUrl}${photo.photo}`}
              alt={`Image ${photo.id}`}
              title={photo.id}
              onClick={() => openLightbox(index)}
              className="cursor-pointer object-cover w-full h-full"
            />
          </div>
        ))}
      </div>

      <Lightbox
        plugins={[Zoom, Fullscreen, Download, Share, Counter, Thumbnails, Slideshow]}
        index={index}
        slides={gallery.map((photo) => ({
          src: `${baseUrl}${photo.photo}`,
          title: `Image ${photo.id}`,
        }))}
        open={index >= 0}
        close={closeLightbox}
      />
    </div>
  );
}

export default Gallery;
