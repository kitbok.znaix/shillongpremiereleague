import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

function QuickSearch() {
  const [team, setTeam] = useState([]);
  const kitPort = 'http://192.168.1.48/ShillongPremiereLeague/Read/R_AllTeam.php';
  const baseUrl = 'http://192.168.1.48/ShillongPremiereLeague/Read/';
  // const fakeAPI ='https://fakestoreapi.com/products';
  useEffect(() => {
    axios
      .get(kitPort)
      .then((res) => {
        if (res.data.data && Array.isArray(res.data.data)) {
          setTeam(res.data.data); // Extract the array from the "data" property
        } else {
          console.error('API response does not contain an array:', res.data);
        }
      })
      .catch((err) => console.error(err));
  }, []);

  return (
    <div>
      <div className="grid-container">
        <div className="grid lg:grid-cols-3 gap-4 mx-40 my-20 md:grid-cols-2 sm:grid-cols-1">
          {team.map((item,index) => (
            <div key={`${item.id}-${index}`} className="bg-white rounded-md shadow-md p-4 ">
              {/* Construct the full image URL */}
              <img
                src={`${baseUrl}${item.teamLogo}`}
                alt="Team Logo"
                className="min-w-fit h-28 ml-20"
              />
              <div className="text-lg font-semibold">{item.teamName}</div>
              <Link to={`/teamDetails/${item.teamId}`}>more info</Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default QuickSearch;
