import React, { useEffect, useState } from "react";
import axios from "axios";

function About() {
  const [about, setAbout] = useState([]);

  const aboutApi =
    "http://192.168.1.48/ShillongPremiereLeague/read/R_About.php";

  useEffect(() => {
    axios
      .get(aboutApi)
      .then((res) => {
        // Assuming that 'data' is an array containing about information
        setAbout(res.data.data); // Update 'about' state with the data array
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);

  const backgroundImageStyle = {
    backgroundImage: "url(hero3.jpeg)",
    backgroundSize: "cover",
    backgroundPosition: "center top",
    minHeight: "calc(500px - 30px)", // Adjust 56px according to your Navbar height
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop:"-10px" 
  };

  return (
    <>
      <div className="mt-14">
        {/* Use map to render each about item */}
        {about.map((item) => (
          <div key={item.id}>
            <h1 className="text-2xl font-bold text-white" style={backgroundImageStyle}>
              {item.title}
            </h1>
            <div className="bg-white p-4 rounded-lg shadow-lg my-4 ">
              <p className="text-gray-700  mx-28 text-justify"
              dangerouslySetInnerHTML={{ __html: item.body }}
              />
            </div>
          </div>
        ))}
      </div>
    </>
  );
}

export default About;
