<?php

include('../code/httpResponse.php');
include('../code/validate.php');
include('../code/token.php');


function updateAll($handleUpdate) {

        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Origin: *"); // You can replace * with specific origins
        header("Access-Control-Allow-Methods: OPTIONS, PUT");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");

    $requestMethod = $_SERVER['REQUEST_METHOD'];

    if ($requestMethod === 'OPTIONS') {
        // Allow the preflight request
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Origin: *"); // You can replace * with specific origins
        header("Access-Control-Allow-Methods: OPTIONS, PUT");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
        exit();
    }

    $token = getToken();
    $tokenValid = verifyToken($token);
    // echo $tokenValid;

    // Check if $tokenValid is a JSON string and decode it into an array
    if (is_string($tokenValid)) {
        $tokenValidDecode = json_decode($tokenValid, true);
    }

    // Check if $tokenValidDecode is an array and has a 'status' key
    if (isset($tokenValidDecode['status']) && $tokenValidDecode['status'] === 200) {
        if($requestMethod === 'PUT') {
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

            if($contentType === 'application/json'){
                $inputData = json_decode(file_get_contents("php://input"), true);
                $updateRecord = $handleUpdate($inputData);
            return $updateRecord;

            } else {
                $data = [
                    'status' => '422',
                    'message' => ' sent the data as json'
                ];
                http_response_code(422); // Set the HTTP response code to 405
                echo json_encode($data);

            }

        } else {
            // Handle invalid request method
            $data = [
                'status' => '405',
                'message' => $requestMethod . ' Method Not Allowed'
            ];
            http_response_code(405); // Set the HTTP response code to 405
            echo json_encode($data);
        }
    } else {
        // Handle the case where the condition is false
        http_response_code(422); // Set the HTTP response code to 422
        echo $tokenValid;
    }
}
?>
