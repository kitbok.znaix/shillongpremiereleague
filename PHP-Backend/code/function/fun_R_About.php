<?php
require('../code/connection.php');

//-------------------READ------------------------// OLD ARRAY DISPLAY
function readAbout()
{
    global $conn; // global variable for db connection
$query = "SELECT * FROM aboutpage";

$stmt = $conn->prepare($query);
$stmt->execute();

    if ($stmt) {
        if ($stmt->rowCount() > 0) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $data = [
                'status' => '200',
                'message' => 'Successful',
                'data' => $result
            ];
                // header('Content-Type: application/json');
                return json_encode($data);
        } else {
            httpResponse(404, 'No results Found!'); // Set the HTTP response code to 422
        }
    } else {
        httpResponse(500, 'Error executing query'); // Set the HTTP response code to 500
    }
}


?>
