<?php
require('../code/connection.php');

//-------------------DELETE------------------------//
    function deleteAllTeam($userInput) {
        global $conn; // global variable for db connection

        $teamIdCheck = empty($userInput['id']) ? httpResponse(422, 'Enter team id') : validateNumber($userInput['id'], 'Invalid id');
        if ($teamIdCheck) {
            $teamId = isElementExists($teamIdCheck,'allteam','id');
        }

        if ($teamId) {
            // Check if the teamId exists in the gameplay table
            $queryCheckTeamInGamePlay = "SELECT * FROM gameplay WHERE teamA=:teamId OR teamB=:teamId";
            $stmtCheckTeamInGamePlay = $conn->prepare($queryCheckTeamInGamePlay);
            $stmtCheckTeamInGamePlay->bindParam(':teamId', $teamId);
            $stmtCheckTeamInGamePlay->execute();
            $teamInGamePlayResult = $stmtCheckTeamInGamePlay->fetch(PDO::FETCH_ASSOC);

            if ($teamInGamePlayResult) {
                httpResponse(422, 'This team cannot be deleted since it is already playing');
            } else {
                // Get the image paths from the 'allteam' and 'teamimage' tables
                $queryGetTeamImages = "SELECT teamLogo, coachPhoto FROM allteam WHERE id=:teamId";
                $stmtGetTeamImages = $conn->prepare($queryGetTeamImages);
                $stmtGetTeamImages->bindParam(':teamId', $teamId);
                $stmtGetTeamImages->execute();
                $teamImages = $stmtGetTeamImages->fetch(PDO::FETCH_ASSOC);

                $queryGetTeamPhoto = "SELECT teamPhoto FROM teamimage WHERE teamId=:teamId";
                $stmtGetTeamPhoto = $conn->prepare($queryGetTeamPhoto);
                $stmtGetTeamPhoto->bindParam(':teamId', $teamId);
                $stmtGetTeamPhoto->execute();

                // Initialize an array to store image paths
                $teamPhotoPaths = [];

                // Iterate through the rows and collect image paths using a while loop
                while ($row = $stmtGetTeamPhoto->fetch(PDO::FETCH_ASSOC)) {
                    $teamPhotoPaths[] = $row['teamPhoto'];
                }

                // Delete the team record from the 'allteam' table
                $queryDeleteAllTeam = "DELETE FROM allteam WHERE id=:teamId";
                $stmtDeleteAllTeam = $conn->prepare($queryDeleteAllTeam);
                $stmtDeleteAllTeam->bindParam(':teamId', $teamId);
                $resultDeleteAllTeam = $stmtDeleteAllTeam->execute();

                // Delete related records from the 'teamimage' table
                $queryDeleteTeamImage = "DELETE FROM teamimage WHERE teamId=:teamId";
                $stmtDeleteTeamImage = $conn->prepare($queryDeleteTeamImage);
                $stmtDeleteTeamImage->bindParam(':teamId', $teamId);
                $resultDeleteTeamImage = $stmtDeleteTeamImage->execute();

                if ($resultDeleteAllTeam && $resultDeleteTeamImage) {
                    // Delete team and related teamimage records successfully

                    // Delete associated image files
                    $teamLogoPath = $teamImages['teamLogo'];
                    $coachPhotoPath = $teamImages['coachPhoto'];
                    // $teamPhotoPath = $teamPhoto['teamPhoto'];

                    deleteImage($teamLogoPath);
                    deleteImage($coachPhotoPath);
                    deleteImage($teamPhotoPaths);

                    httpResponse(200, 'Team and related records deleted successfully');
                } else {
                    httpResponse(500, 'Error deleting team and/or teamimage records');
                }
            }
        }
    }
?>
