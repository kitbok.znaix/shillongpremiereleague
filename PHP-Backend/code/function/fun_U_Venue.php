<?php
require('../code/connection.php');

//-------------------CREATE------------------------//
function updateVenue($userInput) {
    global $conn; // global variable for db connection

    $venueIdCheck = empty($userInput['id']) ? httpResponse(422, 'Enter venue id') : validateNumber($userInput['id'], 'Invalid id');
    
    if ($venueIdCheck) {
        $venueId = isElementExists($venueIdCheck,'place','id');
    } else {
        httpResponse(422, 'Cannot find id on database');
    }

    $venueName = empty($userInput['venueName']) ? httpResponse(422, 'Enter venue venueName') : validateString($userInput['venueName']);

    if ($venueId && $venueName) {
        try {
            $query = "UPDATE place SET venueName = :venueName WHERE id = :venueId";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(':venueName', $venueName);
            $stmt->bindParam(':venueId', $venueId);
            $result = $stmt->execute();

            if ($result) {
                httpResponse(200, 'Venue updated successfully');
            }

        } catch (Exception $e) {
            // Handle the exception here
            httpResponse(500, 'An error occurred: ' . $e->getMessage());
        }
    } else {
        httpResponse(422, 'id, venue id, and venue name are required');
    }
}
?>
