<?php
    require('../code/connection.php');

    //-------------------CREATE------------------------//
    function createVenue($userInput) {
        global $conn; // global variable for db connection
        if (!empty($userInput['venue'])) {
            foreach ($userInput['venue'] as $index => $venue) {
                $validateVenue = validateString($venue);
        
                $query = "INSERT INTO place (venue) VALUES (:venue)";
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':venue', $validateVenue);
        
                $galleryResult = $stmt->execute();
            }

                if ($galleryResult) {
                    $data = [
                        'status' => '201',
                        'message' => 'Venue added Successfully',
                    ];
                    return json_encode($data);
                } else {
                    $data = [
                        'status' => '500',
                        'message' => 'Error executing query',
                    ];
                    return json_encode($data);
                }
        } else {
            httpResponse(422, 'Please Add Venue');
        }
             
    }   

?>
