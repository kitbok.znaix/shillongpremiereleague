<?php
require('../code/connection.php');

//-------------------DELETE------------------------//
function deleteGallery($userInput) {
    global $conn; // global variable for db connection

    $photoIdCheck = empty($userInput['id']) ? httpResponse(422, 'Enter photo id') : validateNumber($userInput['id'], 'Invalid id');
    if ($photoIdCheck) {
        $photoId = isElementExists($photoIdCheck, 'gallery', 'id');
    }

    if ($photoId) {
        // Get the file path of the photo from the database
        $queryGetFilePath = "SELECT photo FROM gallery WHERE id=:photoId";
        $stmtGetFilePath = $conn->prepare($queryGetFilePath);
        $stmtGetFilePath->bindParam(':photoId', $photoId);
        $stmtGetFilePath->execute();
        $resultGetFilePath = $stmtGetFilePath->fetch(PDO::FETCH_ASSOC);

        if ($resultGetFilePath) {
            $filePath = $resultGetFilePath['photo'];

            // Delete the photo file using the deleteImage function
            if (deleteImage($filePath)) {
                // File deletion was successful

                // Delete the photo record from the database
                $queryDeleteGallery = "DELETE FROM gallery WHERE id=:photoId";
                $stmtDeleteGallery = $conn->prepare($queryDeleteGallery);
                $stmtDeleteGallery->bindParam(':photoId', $photoId);

                if ($stmtDeleteGallery->execute()) {
                    // Deletion from both the database and folder was successful
                    httpResponse(200, 'Photo and record deleted successfully');
                } else {
                    // Error occurred during database deletion
                    httpResponse(500, 'Internal Server Error');
                }
            } else {
                // Error occurred during file deletion
                httpResponse(500, 'Error deleting the file');
            }
        } else {
            httpResponse(404, 'Photo not found in the database');
        }
    } else {
        httpResponse(404, 'Photo not found');
    }
}
?>
