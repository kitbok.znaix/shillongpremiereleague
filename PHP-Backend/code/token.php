<?php
    // function generateKey($length = 32) {
    //     return base64_encode(random_bytes($length));
    // }
    
    // // Example usage:
    // $randomString = generateKey(64); // Generate a 64-character random string
    // echo $randomString;
    

    const KEY = 'jJGABJQIR/ZiebyN9bKmXIuR8tkHgEUPnFE/PNNgSd8V0yHc4CsCROZtLfTi7JpqbIlgAsRNbamyOc3Vuc6SCw==';

    function allHeaders() {
        // Get all HTTP request headers
        $headers = getallheaders();
        
        $headerArray = []; // Initialize an empty array to store headers
    
        // Loop through and store each header in the array
        foreach ($headers as $name => $value) {
            $headerArray[$name] = $value;
        }
    
        // Return the array of headers
        return $headerArray;
    }

    // 1st GENERATE A TOKEN
    function generateToken($payload, $expire = null) {
        // Header
        $headers = ['algo' => 'HS256', 'type' => 'JWT'];
        
        // Add the expiration claim only if $expire is provided
        if ($expire !== null) {
            $headers['expire'] = time() + $expire;
        }
    
        $headers_encoded = base64_encode(json_encode($headers));
    
        // Payload
        $payload['time'] = time();
        $payload_encoded = base64_encode(json_encode($payload));
    
        // Signature
        $signature = hash_hmac('SHA256', $headers_encoded . $payload_encoded, KEY);
        $signature_encoded = base64_encode($signature);
    
        // Token
        $token = $headers_encoded . '.' . $payload_encoded . '.' . $signature_encoded;
    
        return $token;
    }
    

    // 2nd GET A TOKEN
    function getToken() {
        // Get all headers as an array
        $headers = allHeaders();
        
        // Check if the Authorization header exists
        if (isset($headers['Authorization'])) {
            $authorizationHeader = $headers['Authorization'];
        
            // Check if the header starts with "Bearer"
            if (strpos($authorizationHeader, 'Bearer ') === 0) {
                // Extract and return the token
                return substr($authorizationHeader, 7);
            }
        }
        
        // Return null if no valid token is found
        return null;
    }
    

    // 3rd VERIFY A TOKEN
    function verifyToken($token) {
        if ($token !== null) {
        // Break token parts
            $token_parts = explode('.', $token);
        
            // Check if all token parts are present in here it's 3
            if (count($token_parts) !== 3) {
                // httpResponse(422, 'Token Invalid number of segments');
                $responseData = [
                    'status' => 422,
                    'message' => 'Token Invalid number of segments',
                    'token' => $token
                ];
                // http_response_code(422);
                return json_encode($responseData);
            }
        
            // Verify Signature check if 1st and 2nd token parts are equal [signature = 3parts]
            $signature = base64_encode(hash_hmac('SHA256', $token_parts[0] . $token_parts[1], KEY));
            if ($signature !== $token_parts[2]) {
                // httpResponse(401, 'Token Signature Invalid');
                $responseData = [
                    'status' => 422,
                    'message' => 'Token Signature Invalid',
                    'token' => $token
                ];
                // http_response_code(422);
                return json_encode($responseData);
            }
        
            // Decode headers & payload
            $headers = json_decode(base64_decode($token_parts[0]), true);
            $payload = json_decode(base64_decode($token_parts[1]), true);
        
            // Verify validity
            if (isset($headers['expire']) && $headers['expire'] < time()) {
                // httpResponse(401, 'Token Expired');
                $responseData = [
                    'status' => 422,
                    'message' => 'Token Expired',
                    'token' => $token
                ];
                // http_response_code(422);
                return json_encode($responseData);
            }
        
            // If token is successfully verified, return the payload
            $responseData = [
                'status' => 200,
                'message' => 'Token valid',
                'payload' => $payload
            ];
            // http_response_code(422);
            return json_encode($responseData);
        } else {
            // httpResponse(422, 'Token is Null');
            $responseData = [
                'status' => 422,
                'message' => 'Token is Null',
                'token' => $token
            ];
            // http_response_code(422);
            return json_encode($responseData);
        }
    }
    

?>
