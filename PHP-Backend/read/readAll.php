<?php

    include('../code/httpResponse.php');
    include('../code/validate.php');

    function readAll($handleRead) {

        $requestMethod = $_SERVER['REQUEST_METHOD'];

        // Allow the preflight request
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type");
        header("Access-Control-Allow-Credentials: true");

        if ($requestMethod === 'OPTIONS') {
            // Allow the preflight request
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: GET, OPTIONS");
            header("Access-Control-Allow-Headers: Content-Type");
            header("Access-Control-Allow-Credentials: true");
            exit();
        }

        header('Content-Type: application/json');
        
        
        if ($requestMethod === 'GET') {
                
            if(isset($_GET['id'])) {
                $team = getSingleRecordTeam($_GET); // Get the SINGLE user RECORD from the database by the user id.
                echo $team;

            } else {
                $recordList = $handleRead();
                echo $recordList;
            }      
                
        } else {
             // Handle invalid request method
            $data = [
                    'status' => '405',
                    'message' => $requestMethod . ' Method Not Allowed'
                    ];
            http_response_code(405); // Set the HTTP response code to 405
            echo json_encode($data);
        }
    }
?>