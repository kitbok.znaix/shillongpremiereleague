<?php

    include('code/connection.php');
    include('code/token.php');
    include('code/httpResponse.php');
    include('code/validate.php');


// Pass the specific function name as an argument to handleRequest
$verifyUser = checkRequestMethod();
echo  $verifyUser;

function checkRequestMethod() {

    // header("Authorization: Bearer $token");
    $requestMethod = $_SERVER['REQUEST_METHOD'];

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type");
        header("Access-Control-Allow-Credentials: true");

    if ($requestMethod === 'OPTIONS') {
        // Allow the preflight request
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type");
        header("Access-Control-Allow-Credentials: true");
        exit();
    }

        if ($requestMethod === 'POST') {
            // Check if Content-Type is application/json
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
            if ($contentType === 'application/json') {
                // Handle JSON input
                $inputData = json_decode(file_get_contents("php://input"), true);
            } else {
                // Handle form data input
                $inputData = $_POST;
                
            }
            $storeRecord = verifyUser($inputData);
            return $storeRecord;
        } else {
            // Handle invalid request method
            $data = [
                'status' => '405',
                'message' => $requestMethod . ' Method Not Allowed'
            ];
            http_response_code(405); // Set the HTTP response code to 405
            return json_encode($data);
        }
}

//-------------------VERIFY USER------------------------//
function verifyUser($userInput) {
    global $conn; // global variable for db connection

    $userName = empty($userInput['userName']) ? httpResponse(422,'Enter User name') : validateString($userInput['userName']);
    $password = $userInput['password'];

    if (empty($password)) {
        httpResponse(422, 'Enter Password');
    }

    if ($userName && $password) {
        try {
            // Retrieve user information from the database based on the username
            // $query = "SELECT userName, password FROM userrole WHERE userName = :userName";
            $query = "SELECT id, userName, password FROM userrole WHERE userName = :userName";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(':userName', $userName);
            $stmt->execute();

            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($user) {
                // Verify the password
                $hashedPassword = $user['password'];

                if (password_verify($password, $hashedPassword)) {
                    // Passwords match, user is verified
                    // Generate TOKEN
                    $dataPayload = ['ID'=>$user['id'],
                                    'UserName' => $user['userName']];

                    $token = generateToken($dataPayload);// Example payload and expiration time
                    $responseData = [
                        'status' => 200,
                        'message' => 'Login successful',
                        'token' => $token
                    ];
                    http_response_code(200);
                    return json_encode($responseData);
                    
                    // Return the token or use it as needed
                    // return $token;
                } else {
                    httpResponse(422, 'Password not match');
                }
            } else {
                httpResponse(422, 'Username not match');
            }
        } catch (PDOException $e) {
            httpResponse(500, 'Database error: ' . $e->getMessage());
        }
    } else {
        httpResponse(422, 'Missing required fields');
    }
}

?>
